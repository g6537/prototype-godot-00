using Godot;
using System;

public class EnemyKinematicArea : KinematicBody2D
{

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta)
  {
      
  }

  public void OnAreaEntered(Area2D area)
  {
      if (area is Bullet2)
      {
          GD.Print("Hit By Bullet2");
      }

      if (area is NaniteFieldBurst)
      {
          GD.Print("Hit by field");
      }
  }
}
