using Godot;
using System;

public class Player_Char_1 : Player
{
    
    public override void _Input(InputEvent inputEvent) {
        var animation = GetNode<AnimationPlayer>("Swordanim");
        // Sword
        if (Input.IsActionJustPressed("Slash"))
        {
            animation.Play("Sword_anim");


        }
        
        // Whip
        if (Input.IsActionJustPressed("Whip"))
        {
            animation.Play("WhipAnim");
        }
    }
    
    private void OnAreaEnteredChar2(PhysicsBody2D body)
  {
      if (body is Enemy)
      {
          GD.Print("Cut Area");
          Enemy.HpDecrease(2);
          
      }
  }
}
