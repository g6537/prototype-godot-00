using Godot;
using System;

public class ApBullet : RigidBody2D
{
    public override void _Ready() {
        GravityScale = 0;
        ContactMonitor = true;
        ContactsReported = 1;
    }

    public void OnScreenExitAp() {
        QueueFree();
    }

    public void OnBodyEnteredAp(PhysicsBody2D body) {
        if (body is Enemy)
        {
            Enemy.HpDecrease(1);
        }
        
        GetNode<CollisionShape2D>("CollisionAp").SetDeferred("disabled", true);
        // send single for score
        Hide();
        // delete it
        QueueFree();

    }
}
