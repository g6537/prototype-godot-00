using Godot;
using System;

public class NaniteFieldBurst : Area2D
{
    #pragma warning disable 649
    [Export] public int Speed;
    #pragma warning restore 649
    private int _direction;
    private Sprite _field;
    private CollisionPolygon2D _collision;
    private RayCast2D _floorRay;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _direction = 1;
        _field = GetNode<Sprite>("NaniteField");
        _collision = GetNode<CollisionPolygon2D>("FieldBox");
        _floorRay = GetNode<RayCast2D>("FloorContact");

    }

 // Called every frame. 'delta' is the elapsed time since the previous frame.
 public override void _Process(float delta)
 {
     Position += Transform.x * Speed * _direction * delta;
     if (!_floorRay.IsColliding())
     {
         Delete();
     }
 }

 // For interacting with enemies and bosses
 public void OnBodyEnteredField(KinematicBody2D body)
 {
     if (body is EnemyKinematicArea)
     {
         GD.Print("Hit by burst field");
     }
     
 }

 // For when the thing hits the wall
 public void OnBodyEntered(PhysicsBody2D body)
 {
     if (!(body is Player_Char_2))
     { 
         Delete();
     }

 }

 public void Flip(bool flip)
 {
     if (flip)
     {
         _direction = -1;
         _field.FlipH = true;
         _collision.Scale = new Vector2(-1, 1);
         _floorRay.Position = Transform.x * -8;

     }
     else
     {
         _direction = 1;
         _field.FlipH = false;
     }
 }

 private void Delete()
 {
     GetNode<CollisionPolygon2D>("FieldBox").SetDeferred("disabled", true);
     Hide();
     QueueFree();
 }
}
