using Godot;
using System;

public class SawPlaceHolder : RigidBody2D
{
    private Vector2 velocity = new Vector2();

    private RayCast2D _floor;
    private RayCast2D _wall;
    private RayCast2D _ceiling;
    public override void _Ready()
    {
        _floor = GetNode<RayCast2D>("RayCastFloor");
        _ceiling = GetNode<RayCast2D>("RayCastCeiling");
        _wall = GetNode<RayCast2D>("RayCastWall");
    }

  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta)
  {
     
 }

  private void traverse()
  {
      if (_floor.IsColliding())
      {
          LinearVelocity = new Vector2(300, 0);
      }

      if (_wall.IsColliding())
      {
          LinearVelocity = new Vector2(0, 300);
      }

      if (_ceiling.IsColliding())
      {
          LinearVelocity = new Vector2(-300, 0);
      }
  }
}
