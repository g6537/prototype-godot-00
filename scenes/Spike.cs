using Godot;
using System;

public class Spike : Sprite
{


    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {

    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {

    }

    private void BodyEntered2D(object body)
    {
        GD.Print("Body: " + body + "has entered" );
        if (body is KinematicBody2D)
        {
            if (body is Player)
            {
                Player pc = body as Player;
                pc.TakeDamage(1);
            }
        }
    }
    
}
