using Godot;
using System;

public class MovingEnemyKinematic : KinematicBody2D
{

    private Vector2 _velocity;
    private RayCast2D _left;
    private RayCast2D _right;
    private RayCast2D _wall;
    private int _direction;
    private int _speed = 20;
    private int _gravity = 50;
    public override void _Ready()
    {
        _velocity = new Vector2(0,0);
        _direction = -1;
        _left = GetNode<RayCast2D>("RayLeft");
        _right = GetNode<RayCast2D>("RayRight");
        _wall = GetNode<RayCast2D>("RayWall");
    }

    public override void _Process(float delta)
    {
        Movement();
        Turning();

        if (_wall.IsColliding())
        {
            GD.Print("is true");
            _wall.CastTo = new Vector2(8 * _direction, 0);
        }
        Gravity(delta);

        MoveAndSlide(_velocity);
    }

    private void Gravity(float delta)
    {
        _velocity.y += _gravity;
    }

    private void Movement()
    {
        _velocity.x = _speed * _direction;
    }

    private void Turning()
    {
        if (!_left.IsColliding())
        {
            _direction = 1;
        } else if (!_right.IsColliding())
        {
            _direction = -1;
        }
    }

    public void OnBodyEnteredArea(PhysicsBody2D body)
    {
        if (body is Player)
        {
            GD.Print("Player Collided");
        }

    }

    public void OnAreaEntered(Area2D area)
    {
        if (area is Bullet2)
        {
            GD.Print("Hit by Bullet2");
        }

        if (area is NaniteFieldBurst)
        {
            GD.Print("Hit by field");
        }
    }

}
