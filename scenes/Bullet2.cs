using Godot;
using System;

public class Bullet2 : Area2D
{
    private static Main.Elements _currentElement;
    private int _directionalControl;
    public override void _Ready()
    {
        SetElement("Ice");
    }

    public override void _PhysicsProcess(float delta)
    {
        Position += Transform.x * 500 * delta*_directionalControl;
    }
    
    public static Main.Elements GetElement()
    {
        return _currentElement;
    }
    
    public static void SetElement(String element)
    {
        switch (element)
        {
            case "Fire":
            {
                _currentElement = Main.Elements.Fire;
                break;  
            }

            case "Ice":
            {
                _currentElement = Main.Elements.Ice;
                break;
            }

            case "Electricity":
            {
                _currentElement = Main.Elements.Electricity;
                break;
            }

            default:
            {
                _currentElement = Main.Elements.Neutral;
                break;
            }

        }
    }
    
    public void DirectionControl(int direction)
    {
        _directionalControl = direction;
    }
    
    public void OnBodyEntered(PhysicsBody2D body) {
        
        GetNode<CollisionShape2D>("Collision").SetDeferred("disabled", true);
        // send single for score
        Hide();
        // delete it
        QueueFree();

    }

    public void OnAreaEntered(Area2D area)
    {
        GetNode<CollisionShape2D>("Collision").SetDeferred("disabled",true);
        
        Hide();
        
        QueueFree();
    }

    public void OnScreenExited()
    {
        QueueFree();
    }
}
