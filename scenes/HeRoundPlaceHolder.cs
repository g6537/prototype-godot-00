using Godot;
using System;

public class HeRoundPlaceHolder : RigidBody2D
{
    private Sprite _sprite;
    public override void _Ready() {
        GravityScale = 0;
        ContactMonitor = true;
        ContactsReported = 1;
        _sprite = GetNode<Sprite>("Sprite");

    }

    public void OnScreenExitHe() {
        QueueFree();
    }

    public async void OnBodyEnteredHe(PhysicsBody2D body) {
        if (body is Enemy)
        {
            LinearVelocity = new Vector2(0, 0);
            Enemy.HpDecrease(1);
        }

        Timer explosionTimer = GetNode<Timer>("ExplosionTimer");
        
        _sprite.Hide();

        GetNode<AnimationPlayer>("ExplosionAnimation").Play("ExplosionAnim");
        await ToSignal(explosionTimer, "timeout");

        GetNode<CollisionShape2D>("CollisionHe").SetDeferred("disabled", true);
        //hide it
        Hide();
        // delete it
        QueueFree();


    }

    public async void BodyCaught(PhysicsBody2D body)
    {
        if (body is Enemy)
        {
            Enemy.HpDecrease(1);
            GD.Print("Caught in explosion");
        }
    }

}

