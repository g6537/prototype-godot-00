using Godot;
using System;

public class WallHitBox : Area2D
{
    public override void _Ready()
    {

    }

    public void OnBodyEnteredIce(Area2D body)
    {
        GD.Print("Is Hit");
        GD.Print(body.GetType());

        if (body is Bullet2)
        {
            if (Bullet2.GetElement() == Main.Elements.Ice)
            {
                GD.Print("Correct Element");
                Hide();
                QueueFree();
            }
        }

    }
}
