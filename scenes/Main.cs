using Godot;
using System;

public class Main : Node2D
{
#pragma warning disable 649
    [Export] public PackedScene Bullet2Scene;
    [Export] public PackedScene BulletScene;
    [Export] public PackedScene ApBulletScene;
    [Export] public PackedScene HeRoundPlaceHolderScene;
    [Export] public PackedScene ChargedBulletScene;
#pragma warning restore 649

    private Player _player;
    private Player_Char_2 _player1;
    private Timer _apTimer;
    private Timer _chargeTimer;
    private int _direction;
    
    public override void _Ready()
    {
        _player1 = GetNode<Player_Char_2>("Player_Char_2");
        _apTimer = GetNode<Timer>("APBulletTimer");
        _chargeTimer = GetNode<Timer>("ChargeTimer");
        _direction = 1;

    }

    public enum Elements
    //Anything and everything that has or doesn't have an element in it uses this enum
    {
        Fire=0,
        Ice=1,
        Neutral=4,
        Electricity=3
    }


    public override void _Process(float delta)
    {
        
    }
    
    // The method for the High Explosive bullet
    private void HeFire()
    {
        var explosive = (HeRoundPlaceHolder) HeRoundPlaceHolderScene.Instance();
        AddChild(explosive);
        
        explosive.Position = _player1.Position;

        explosive.LinearVelocity = new Vector2(500f, 0);
    }
    
    // The method for the regular bullet
    private void BulletFire() {
        var bullet = (Bullet2) Bullet2Scene.Instance();
        AddChild(bullet);

        

        bullet.Position = _player1.Position;
        if (Input.IsActionPressed("ui_left") || Input.IsActionJustPressed("ui_left"))
        {
            _direction = -1;
        }
        else if(Input.IsActionPressed("ui_right") || Input.IsActionJustPressed("ui_right"))
        {
            _direction = 1;
        }
        
        bullet.DirectionControl(_direction);

    }
    
    // The method for the Charged bullet
    private void ChargedFire()
    {
        if (!Input.IsActionJustReleased("Fire"))
        {
            var charged = (ChargedBullet) ChargedBulletScene.Instance();
            AddChild(charged);
            charged.Position = _player1.Position;
            charged.LinearVelocity = new Vector2(300f, 0);
            _chargeTimer.Stop();  
        }
        else
        {
            _chargeTimer.Stop();
        }

    }
    
    // The method for the Armor Piercing bullet
    private void ApFire()
    {
     
        var apBullet = (ApBullet) ApBulletScene.Instance();
    
        AddChild(apBullet);
     
        var direction = 1;

        apBullet.Position = _player1.Position;
     
        if (Input.IsActionPressed("ui_left"))
        {
            direction = -1;
        }

        apBullet.LinearVelocity = new Vector2(300f * direction, 0);
     
    }
    // This is where the firing of the bullets takes place
    public override void _Input(InputEvent inputEvent) {
     
        if (inputEvent.IsActionPressed("Fire"))
        {
            BulletFire();
            _chargeTimer.Start();
        }

        if (inputEvent.IsActionPressed("HeRound"))
        {
            HeFire();
        }

        if (inputEvent.IsActionPressed("AP_Fire")) {
            _apTimer.Start();
        }

        if (inputEvent.IsActionReleased("AP_Fire")) {
            _apTimer.Stop();
        }
    }
 
}