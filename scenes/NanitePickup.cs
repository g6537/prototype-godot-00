using Godot;
using System;

public class NanitePickup : Area2D
{
    
    public void OnBodyEnteredRestore(KinematicBody2D body)
    {
        if (body is Player_Char_2)
        {
            Delete();
        }
    }

    private void Delete()
    {
        GetNode<CollisionShape2D>("HitBox").SetDeferred("disabled", true);
        Hide();
        QueueFree();
    }
}
