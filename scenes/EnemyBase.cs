using Godot;
using System;

public class EnemyBase : Node
{
/*
 * I'm creating this class so that I can make it easier for myself to make Godot
 * identify enemies without being constrained to the _Ready() and _Process functions
 * since I generally want the enemies to be varied.
 *
 * Hopefully this ends up being useful in the future and doesn't shoot me in the
 * foot as I move forward. Despite the little progress made over the last couple of months. >_>
 */

 public void SetHealth()
 {
  
 }

 public int GetHealth()
 {
  // Just to let it compile for now
  return 0;
 }
}
