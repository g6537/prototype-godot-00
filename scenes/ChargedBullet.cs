using Godot;
using System;

public class ChargedBullet : Bullet
{
    public override void _Ready() {
        GravityScale = 0;
        ContactMonitor = true;
        ContactsReported = 1;
    }

    public void OnScreenExit() {
        QueueFree();
    }

    public void OnBodyEntered(PhysicsBody2D body) {
        
        GetNode<CollisionShape2D>("Collision").SetDeferred("disabled", true);
        // send single for score
        Hide();
        // delete it
        QueueFree();

    }

}
