using Godot;
using System;

public class TurretEnemy : Area2D
{
#pragma warning disable 649;
    [Export] public PackedScene Bullet2Scene;
    [Export] public int Health;
#pragma warning restore 649;

    private Sprite _sprite;
    private Timer _timer;
    private int _direction;
    private Area2D _detection;

    public override void _Ready()
    {
        _sprite = GetNode<Sprite>("Turret");
        _timer = GetNode<Timer>("FiringTimer");
        _detection = GetNode<Area2D>("PlayerDetection");
        _direction = -1;
        Health = 3;
    }

    public override void _Process(float delta)
    {
        if (Health <= 0)
        {
            Delete();
        }
    }

    private void Flip(KinematicBody2D body)
    {
        if (body is Player_Char_2)
        {
            _sprite.FlipH = PlayerLocation();
            _detection.Scale = new Vector2(1 * _direction, 1);
            Flipping();
        }
    }

    public async void TurretTimedFire()
    {
        _timer.Autostart = true;
        await ToSignal(_timer, "timeout");
        Fire();
    }

    private void Fire()
    {
        var bullet = (Bullet2) Bullet2Scene.Instance();
        AddChild(bullet);
        bullet.Position = _sprite.Position;
        bullet.DirectionControl(_direction);
    }

    private void Flipping()
    {
        _direction *= -1;
    }

    private bool PlayerLocation()
    {
        return _direction < 0;
    }

    private void Delete()
    {
        Hide();
        QueueFree();
    }

    public void OnAreaEntered(Area2D area)
    {
        if (area is Bullet2)
        {
            Health--;
        }
    }

}
