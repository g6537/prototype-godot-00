﻿using Godot;
using System;
namespace TitanomachyPrototype.scenes

{
    public interface IDamageEnemy
    {
        void DamagePlayer(int damageTaken);
    }
}