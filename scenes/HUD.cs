using Godot;
using System;

public class HUD : CanvasLayer
{
    private static Label _healthLabel;
    private static Label _naniteLabel;

    public override void _Ready()
    {
        _naniteLabel = GetNode<Label>("NaniteCount");
        _healthLabel = GetNode<Label>("Health");
        UpdateHealth(Player.CurrentHealth());
        UpdateNaniteCount(3);
    }

    public static void UpdateHealth(int health)
    {
        _healthLabel.Text = health.ToString();
    }

    public static void UpdateNaniteCount(int naniteCount)
    {
        _naniteLabel.Text = naniteCount.ToString();
    }

}
