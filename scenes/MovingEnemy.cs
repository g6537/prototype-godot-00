using Godot;
using System;

public class MovingEnemy : Enemy
{
    private Vector2 _velocity;
    private RayCast2D _collideLeft;
    private RayCast2D _collideRight;
    private RayCast2D _collideWallRight;
    private int speed = 50;
    private int direction = 1 ;
    private int gravity = 500;
    public override void _Ready()
    {
        _collideWallRight = GetNode<RayCast2D>("RayWallRight");
        _collideLeft = GetNode<RayCast2D>("RayLeft");
        _collideRight = GetNode<RayCast2D>("RayRight");
        
        SetHP(5);
    }
    
    public override void _Process(float delta)
    {
        _ProcessMovement(delta);
       // _ProcessGravity(delta);
    }
    // Here is how the enemy moves left and right
    private void _ProcessMovement(float delta)
    {
        
        if (_collideRight.IsColliding() && _collideLeft.IsColliding())
        {
            
            _velocity.x += (speed*direction)*delta;
        }
        else
        {
            direction *= -1;
        }
    }
    // This has been commented out due to it being unused because the enemy never falls off the platform it is on
    /*
    private void _ProcessGravity(float delta)
    {
        _velocity.y += gravity;
    }*/

    // To simulate collision damage, must be done via interface instead
    public void OnBodyEnteredArea(PhysicsBody2D body)
    {
        if (body is Player)
        {
            GD.Print("Player Collided");
        }
    }


}
