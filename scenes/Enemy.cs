using Godot;
using System;

public class Enemy : RigidBody2D
{
    //the Health of the enemy
    private static int HP = 2;
    public override void _Ready()
    {
        GravityScale = 2;
        ContactMonitor = true;
        ContactsReported = 1;
        Mass = 10;
        ContinuousCd = CCDMode.CastShape;
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta)
  {
      
  }

  public void OnBodyEntered(PhysicsBody2D body)
  {
      
      if (body is Bullet && !(body is TileMap))
      {
          GD.Print("Is hit?");
          
          
          HpDecrease(1);
          if (HP <= 0)
          {
              GD.Print("Hit");
              Death();
          }
              
          

      }
  }

  public static void HpDecrease(int decreaseby)
  {
      HP-=decreaseby;
      
  }
  public void OnAreaBodyEntered(Area2D area)
  {
      
      if (area.Name == "slashHit")
      {
          GD.Print("Cut");
          HpDecrease(1);
          
      }
  }

  public void SetHP(int totalHp)
  {
      HP = totalHp;
  }

  private void Death()
  {
      GetNode<CollisionShape2D>("EnemyHitBox").SetDeferred("disabled", true);
      Hide();
      QueueFree();
  }
}
