using Godot;
using System;

public class Player_Char_2 : Player
{
#pragma warning disable 649;
    [Export] public PackedScene NaniteFieldBurstScene;
    [Export] public int SlamSpeed;
    [Export] public int BurstCount;
#pragma warning restore 649;
    private Sprite _sprite;
    private float _executionTimerFloat = 3f;
    private int _burstCount = 3;


    private Timer _executeTimer;
    private bool _exceutionIsCharged;
    private NaniteFieldBurst readyField1;
    private NaniteFieldBurst readyField2;

    public void NaniteSlam()
    {
        _sprite = GetNode<Sprite>("Sprite");
        // creates the first field
        var field1 = (NaniteFieldBurst) NaniteFieldBurstScene.Instance();
        //creates the second field
        var field2 = (NaniteFieldBurst) NaniteFieldBurstScene.Instance();
        // should flip the second field generated
        AddChild(field1);
        AddChild(field2);
        field2.Flip(true);
        field1.Position = _sprite.Position;
        field2.Position = _sprite.Position;
        field1.Position += Transform.y * -3;
        field2.Position += Transform.y * -3;

    }

    public override void _Ready()
    {
        CustomReady();
        readyField1 = (NaniteFieldBurst) NaniteFieldBurstScene.Instance();
        readyField2 = (NaniteFieldBurst) NaniteFieldBurstScene.Instance();
    }

    public override void _Input(InputEvent inputEvent)
    {
        if (Input.IsActionPressed("down") && _burstCount > 0)
        {
            Velocity.y += SlamSpeed;
            NaniteSlam();
            GD.Print("Bursts remaining: " + BurstCount);
            _burstCount--;
            HUD.UpdateNaniteCount(_burstCount);
        }

        if (Input.IsActionJustPressed("ExecutionA"))
        {
            _executeTimer = GetNode<Timer>("ExecuteTimer");
            _executeTimer.Start();
            //this is for debugging
            GD.Print(_executeTimer.TimeLeft);
        } else if (Input.IsActionJustReleased("ExecutionA") && _executeTimer.TimeLeft > 0)
        {
            GD.Print(_executeTimer.TimeLeft);
            GD.Print("Statement has reached true");
            _executeTimer.Stop();
            _executeTimer.WaitTime = 3;
        }

        if (_exceutionIsCharged)
        {
            GD.Print("fire");
            _executeTimer.Stop();
            if (inputEvent.IsActionReleased("ExecutionA"))
            {
               ExecutionA(); 
               _exceutionIsCharged = false;
            }
            
        }
    }

    public void RestoreBurst(Area2D pickup)
    {
        if (pickup is NanitePickup)
        {
            BurstCount = 3;
        }
    }

    public void ExecutionA()
    {
        GD.Print("Is being called");
        {
            GD.Print("Is true");
            var executionShape = GetNode<CollisionShape2D>("ExecutionBox/ExecutionShape");
            var executionBox = GetNode<Area2D>("ExecutionBox");

            GD.Print("executed");
            executionShape.Disabled = false;
            Velocity.x = Mathf.Lerp(Velocity.x, Direction * 1000, 1);
            executionBox.Scale = new Vector2(Direction, 1);

            // I'll deal with this segment in a while.

            GD.Print("Shape should be disabled");
            executionShape.Disabled = true;
        }

    }

    private void ExecutionAreaEntered(KinematicBody2D area)
    {
        if (area is EnemyKinematicArea)
        {
            GD.Print("Executed Enemy");
        }
    }
    
    public void ExecuteTimerTimeout()
    {
        _exceutionIsCharged = true;
    }

}