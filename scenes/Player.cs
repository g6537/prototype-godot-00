using Godot;
using System;

public class Player : KinematicBody2D
{
    //Here is where the movement variables start
    private int _gravity = 500;
    private int _speed = 100;
    private float _friction = 0.2f;
    private float _acceleration = 0.25f;
    private int _jumpHeight = 200;
    private int _dashSpeed = 250;
    private static int _health = 5;
    
    private bool _isDashing = false;
    private Vector2 Velocity;
    protected int Direction;
    private float _dashTimer = 0.2f;
    private float _dashTimerReset = 0.2f;
    private bool _isDashAvailable = true;
    private bool _isWallJumping;
    private float _wallJumpTimer = 0.4f;
    private float _wallJumpTimerReset = 0.4f;
    private bool _isFalling;
    //Here is where the movement variables end

    private int _damage = 1;
    //Fire Chip Powerup
    private bool _FireChip;
    //Ice Chip Powerup
    private bool _IceChip;
    //Hover Powerup, needs a function in the future such that upon unlocking
    //the player is granted access to the Hover mechanic
    private bool _Hover;

    private Sprite _sprite;
    private RayCast2D _castLeft;
    private RayCast2D _castRight;
    

    // Called when the node enters the scene tree for the first time.
    public void CustomReady()
    {
        _sprite = GetNode<Sprite>("Sprite");
        _castLeft = GetNode<RayCast2D>("RayCastLeft");
        _castRight = GetNode<RayCast2D>("RayCastRight");
        _Hover = true;
        _FireChip = true;
        _IceChip = true;
        Direction = 1;

    }

  // Called every frame. 'delta' is the elapsed time since the previous frame.
  // Here is where the code for movement starts.
  public override void _Process(float delta)
  {
      
       if (!_isDashing && !_isWallJumping)
       {
           ProcessMovement(delta);
       }

       ProcessWallJump(delta);
       ProcessDash(delta);
       Hover(delta);
     
      if (_isDashing)
      {
          
          _dashTimer -= delta;
          if (_dashTimer <= 0)
          {
              _isDashing = false;
              Velocity = new Vector2(0, 0);
          }
      }
      else
      {
          Velocity.y += _gravity * delta;
      }
      
      if (Velocity.y > 0)
      {
          _isFalling = true;
      }
      else
      {
          _isFalling = false;
      }
      
      Velocity = MoveAndSlide(Velocity, Vector2.Up);
      
      
  }
  
  private void ProcessDash(float delta)
  {
      if (_isDashAvailable)
      {
          if (Input.IsActionPressed("dash"))
          {
              if (Input.IsActionPressed("ui_left"))
              {
                  Velocity.x = -_dashSpeed;
                  _isDashing = true;
              }

              if (Input.IsActionPressed("ui_right"))
              {
                  Velocity.x = _dashSpeed;
                  _isDashing = true;
              }

              if (Input.IsActionPressed("ui_up"))
              {
                  Velocity.y = -_dashSpeed;
                  _isDashing = true;
              }

              if (Input.IsActionPressed("ui_up") && Input.IsActionPressed("ui_right"))
              {
                  Velocity.x = _dashSpeed;
                  Velocity.y = -_dashSpeed;
                  _isDashing = true;
              }

              if (Input.IsActionPressed("ui_up") && Input.IsActionPressed("ui_left"))
              {
                  Velocity.x = -_dashSpeed;
                  Velocity.y = -_dashSpeed;
                  _isDashing = true;
              }

              
              _isDashAvailable = false;
              _dashTimer = _dashTimerReset;
          }
         
      }

  }

  private void ProcessMovement(float delta)
  {
      
      if (!_isDashing)
      {
          int direction = 0;
          
          if (Input.IsActionPressed("ui_left"))
          {
              _sprite.Scale = new Vector2(-1, 1);
              direction = -1;
              Direction = -1;
          }

          if (Input.IsActionPressed("ui_right"))
          {
              _sprite.Scale = new Vector2(1, 1);
              direction = 1;
              Direction = 1;
          }
          

          if (direction != 0)
          {
              Velocity.x = Mathf.Lerp(Velocity.x, direction * _speed, _acceleration);
          }
          else
          {
              Velocity.x = Mathf.Lerp(Velocity.x, 0, _friction);
          }
      }

      if (IsOnFloor())
      {
          if (Input.IsActionPressed("ui_up"))
          {
              Velocity.y = -_jumpHeight;
          }

          _isDashAvailable = true;
      }
      
  }

  private void ProcessWallJump(float delta)
  {
      if (Input.IsActionJustPressed("ui_up") && _castLeft.IsColliding() && Input.IsActionPressed("ui_left") && !IsOnFloor())
      {
          Velocity.y = -_jumpHeight;
          Velocity.x = _jumpHeight;
          _isWallJumping = true;

      } else if (Input.IsActionJustPressed("ui_up") && _castRight.IsColliding() && Input.IsActionPressed("ui_right") && !IsOnFloor())
      {
          Velocity.y = -_jumpHeight;
          Velocity.x = -_jumpHeight;
          _isWallJumping = true;
      }

      if (_isWallJumping)
      {
          _wallJumpTimer -= delta;
          if (_wallJumpTimer <= 0)
          {
              _isWallJumping = false;
              _wallJumpTimer = _wallJumpTimerReset;
          }
      }   
  }
  
  private void Hover(float delta)
  {
      
      if (!IsOnFloor() && _isFalling && Input.IsActionPressed("ui_up"))
      {
          _gravity = 30;
          Velocity.y += _gravity * delta;
      }
      else
      {
          //reset gravity back to normal
          _gravity = 500;
      }
      
  }
  // Here is where the code for handling movement ends
  // Here is where the code for managing health and mana/the special meter should start
  
  // Here is the code for health and damage

  public void TakeDamage(int damage)
  {
      _health -= damage;
      Velocity = MoveAndSlide(new  Vector2());
  }

  public static int CurrentHealth()
  {
      return _health;
  }
    
}
