using Godot;
using System;

public class FireWall : RigidBody2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        ContactMonitor = true;
        ContactsReported = 1;
        ContinuousCd = CCDMode.CastShape;
    }

    public void OnBodyEnteredWall(PhysicsBody2D body)
    {
        // The lines commented out below were for debugging purposes
        //GD.Print((int) (Main.Elements.Ice));
        //GD.Print(Bullet.GetElement());
        //GD.Print("Wall is hit");
        //GD.Print(body.GetType());
        if (body is Bullet)
        {
            GD.Print("Bullet has entered");
            GD.Print(Bullet.GetElement());
            if (Bullet.GetElement() == Main.Elements.Ice)
            {
                GD.Print("Element is correct");
                Hide();
                QueueFree();
            }
        }
    }

    public void OnAreaEnteredWall(Area2D area)
    {
        if (area is Bullet2)
        {
            GD.Print("Bullet2 has Entered");

            if (Bullet2.GetElement() == Main.Elements.Ice)
            {
                GD.Print("Element is Correct");
                Hide();
                QueueFree();
            }
        }
    }
}
