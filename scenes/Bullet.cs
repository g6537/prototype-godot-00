using System;
using Godot;

public class Bullet : RigidBody2D
{

    private static Main.Elements _currentElement;

    public override void _Ready() {
        GravityScale = 0;
        ContactMonitor = true;
        ContactsReported = 1;
        SetElement("Ice");
    }

    public void OnScreenExit() {
        QueueFree();
    }

    public static Main.Elements GetElement()
    {
        return _currentElement;
    }

    public static void SetElement(String element)
    {
        switch (element)
        {
            case "Fire":
            {
                _currentElement = Main.Elements.Fire;
                break;
            }

            case "Ice":
            {
                _currentElement = Main.Elements.Ice;
                break;
            }

            case "Electricity":
            {
                _currentElement = Main.Elements.Electricity;
                break;   
            }

            default:
            {
                _currentElement = Main.Elements.Neutral;
                break; 
            }

        }
    }

    public void OnBodyEntered(PhysicsBody2D body) {
        
        GetNode<CollisionShape2D>("Collision").SetDeferred("disabled", true);
        // send single for score
        Hide();
        // delete it
        QueueFree();

    }

}
